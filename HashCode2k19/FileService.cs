﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HashCode2k19
{
    class FileService
    {
        public List<string> GetListOfLinesFromFile(string filename)
        {
            List<string> lines = new List<string>();
            using (StreamReader stream = new StreamReader(filename))
            {
                while (!stream.EndOfStream)
                {
                    lines.Add(stream.ReadLine());
                }
            }
            return lines;
        }

        public List<string> GetSplittedStringsFromLine(string line, char splittingChar)
        {
            return line.Split(splittingChar).ToList();
        }
    }
}
