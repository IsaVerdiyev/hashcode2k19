﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HashCode2k19
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "a_example.in";
            FileService fileService = new FileService();
            List<string> lines = fileService.GetListOfLinesFromFile(fileName);
            
                var splittedStrings = fileService.GetSplittedStringsFromLine(lines.First(), ' ');
            foreach(var splitted in splittedStrings)
            {
                Console.WriteLine(splitted);
            }
            
        }
    }
}
